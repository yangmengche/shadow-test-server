'use strict'
var express = require('express');
var router  = express.Router();

function test1(req, res, next){
  console.debug("/test1")
  // utils.fnResponse(null, {"test": "ok"}, res, req);
  res.locals={
    "status":null,
    "statusCode":200,
    "body":{"test": "ok"}
  }  
  next();
}

router.get('/test1',  test1);

module.exports = router;
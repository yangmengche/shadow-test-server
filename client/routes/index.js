var express = require('express');
var router = express.Router();
const stClient = require('../shadow-test-client/client');
const path=require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});



function test1(req, res, next){
  console.debug("/test1")
  // utils.fnResponse(null, {"test": "ok"}, res, req);
  res.locals={
    "status":null,
    "statusCode":200,
    "body":{"test": "ok"}
  }
  console.info("res=", res.locals.body);
  next();
}

function test2(req, res, next){
  console.debug("/test2")
  // utils.fnResponse(null, {"test": "ok"}, res, req);
  let now = new Date().getTime();
  res.locals={
    "status":null,
    "statusCode":200,
    "body":{"time": now}
  }  
  console.info("res=", res.locals.body);
  next();
}

router.get('/test1',  test1);
router.get('/test2',  test2);

module.exports = router;

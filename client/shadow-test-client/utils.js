'use strict';
const succCode = require('./statusCode').SuccessCode;

class Utils {
  static fnGetBody(req) {
    return new Promise((resolve, reject) => {
      if (req.readable) {
        let data = '';
        req.setEncoding('utf8');
        req.on('data', function (chunk) {
          data += chunk;
        });
        req.on('end', function () {
          let obj;
          if (data.length > 0) {
            try {
              obj = JSON.parse(data);
            } catch (err) {
              colsole.error('err=', err.message);
              return reject();
            }
          }
          req.body = obj;
          resolve(obj);
        });
      } else {
        resolve(req.body);
      }
    });
  }

  static fnResponse(status, data, res, req) {
    if (!res)
      return;
    if (status) {
      if (status.code) {
        res.status(status.code);
        if (-1 !== Object.keys(succCode).indexOf(status.id) && data && typeof data === 'object') {
          let raw = JSON.stringify(data);
          res.setHeader('Content-Type', 'text/JSON');
          res.setHeader('Content-length', Buffer.byteLength(raw, 'utf8'));
          res.write(raw, 'utf8');
        } else {
          delete status.prototype;
        }
      } else {
        res.status(500);
      }
    } else {
      res.status(200);
      if (data){
        if(typeof data === 'object'){
          let raw = JSON.stringify(data);
          res.setHeader('Content-Type', 'text/JSON');
          res.setHeader('Content-length', Buffer.byteLength(raw, 'utf8'));
          res.write(raw, 'utf8');
        }else{
          res.write(data, 'utf8');
        }
      } 
    }
    // console.debug('header=', res._headers);
    // console.debug('body=', data);
    res.end();
  }
}

module.exports = Utils;
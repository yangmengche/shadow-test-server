## Examples

in app.js

```js
const shClient = require('./shadow-test-client/client');
```

add route at the end of all router
```js
app.use('/', indexRouter);
app.use('/users', usersRouter);

app.post('/enableTest', shClient.enableTest);
app.use(shClient.testRoute);


```

## API
### POST /enableTest
#### body
* enable: boolean. enable or disable test
* server: object of shadow testing server, optional.
* server.host: host of rpc server, defualt is localhost
* server.port: port of rcp server, default is 8000.

example
``` json
{
  enable: true,
  server: {
    host: locahost,
    port: 8000
  }
}
```
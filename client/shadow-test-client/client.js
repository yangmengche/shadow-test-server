'use strint'
const rpc = require('json-rpc2')

const config = require('./appConfig.json');
const utils = require('./utils');

class ShadowTestClient {
  constructor() {
    this.client = null;
    this.testRoute = this.testRoute.bind(this);
    this.enableTest = this.enableTest.bind(this);
  }

  async connect2Server(cfg) {
    return new Promise((resolve, reject) => {
      if(!!cfg){
        config.rpcServer.host = !!cfg.host ? cfg.host: config.rpcServer.host;
        config.rpcServer.port = !!cfg.port ? cfg.port: config.rpcServer.port;
      }
      console.info('[SHClient]','rpc server: ', config.rpcServer.host, config.rpcServer.port);
      this.client = rpc.Client.$create(config.rpcServer.port, config.rpcServer.host);
      this.client.connectSocket((err, conn) => {
        if (err) {
          console.error('[SHClient]',err.message);
          return reject(err);
        }
        if (!!conn) {
          // conn.ended = false;
          conn.on('connect', () => {
            console.info('[SHClient]','rpc connected to', config.rpcServer.host, config.rpcServer.port);
            // this.client.call("shadowTest.route", ["aaa"], (err)=>{
            //   if(!!err){
            //     console.error(err);
            //   }
            // });            
            return resolve();
            // 
          });
        }else{
          console.error('[SHClient]','rpc connection not exist');
          return reject();
        }
      });
    });
  }

  async enableTest(req, res){
    try{
      let body = await utils.fnGetBody(req);
      config.isEnableTest = body.enable;
      if(body.enable === true){
        this.connect2Server(body.server);
      }
    }catch(err){
      console.error('[SHClient]',err);
    }
    console.info('[SHClient]','isEnableTest=', config.isEnableTest);
    utils.fnResponse(null, null, res, req);    
  }

  testRoute(req, res) {
    utils.fnResponse(res.locals.status, res.locals.body, res, req);
    if(config.isEnableTest){
      this.sendTestReq(req, res.locals);
    }
  }



  sendTestReq(req, resLocals){
    try {
      let reqClone = {
        "body": JSON.parse(JSON.stringify(req.body)),
        "headers": JSON.parse(JSON.stringify(req.headers)),
        // "host": JSON.parse(JSON.stringify(req.host)),
        "hostname": JSON.parse(JSON.stringify(req.hostname)),
        "path": JSON.parse(JSON.stringify(req.originalUrl)),
        "method": JSON.parse(JSON.stringify(req.method))        
      }
      if (!!this.client) {
        this.client.call('shadowTest.routeTest', [reqClone, resLocals], (err, data) => {
          if (!!err) {
            console.error('[SHClient]',err);
          }
        });
        // console.log('rpc send:', reqClone);
      }
    } catch (err) {
      console.error('[SHClient]',err.message);
    }
  }
}

let rpcClient = new ShadowTestClient();
module.exports = rpcClient;


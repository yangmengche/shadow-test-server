"use strict";

/**
 * Error table. This module define the error ID and message.
 * @namespace ErrorCode
 */

 var ErrorCode ={
  /**
   * @apiDefine ParameterError
   * @apiError ParameterError Input parameters are not correct.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 400 Bad Request
   *    {'code':400, 'id':'ParameterError', 'message':'Parameter error.'}
   */
 	/** parameter error.*/
  'ParameterError':{'code':400, 'id':'ParameterError', 'message':'Parameter error.'},

  /**
   * @apiDefine LockedAccount
   * @apiError LockedAccount Account has locked.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 400 Bad Request
   *    { 'code': 400, 'id': 'LockedAccount', 'message': 'Locked account.' }
   */
 	/** parameter error.*/
  'LockedAccount': { 'code': 400, 'id': 'LockedAccount', 'message': 'Locked account.' },
      
  /**
   * @apiDefine PayloadError
   * @apiError PayloadError The payload contains unacceptable items.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 409 Conflict
   *    {'code':409, 'id':'PayloadError', 'message':'The payload contains unacceptable items.'}
   */
 	/** parameter error.*/
  'PayloadError':{'code':409, 'id':'PayloadError', 'message':'The payload contains unacceptable items.'},
  
  /**
   * @apiDefine Unauthorized
   * @apiError Unauthorized User is not login or access key is unavaiable.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 401 Unauthorized
   *    {'code':401, 'id':'Unauthorized', 'message':'Unauthorized.'}
   */
  /** Unauthorized .*/
  'Unauthorized':{'code':401, 'id':'Unauthorized', 'message':'Unauthorized.'},

  /**
   * @apiDefine Unauthorized
   * @apiError Unauthorized User is not login or access key is unavaiable.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 401 Unauthorized
   *    {'code':401, 'id':'Unauthorized', 'message':'Unauthorized.'}
   */
  /** Unauthorized .*/
  'PermissionDenied':{'code':401, 'id':'Permission denied', 'message':'Permission denied.'},

  /**
   * @apiDefine NotSupport
   * @apiError NotSupport Request not support.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 405 Method Not Allowed
   *    {'code':405, 'id':'NotSupport', 'message':'Request not support.'}
   */
 	/** Request not support.*/
 	'NotSupport':{'code':405, 'id':'NotSupport', 'message':'Request not support.'},

  /**
   * @apiDefine Forbidden
   * @apiError Forbidden Access forbidden.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'Forbidden', 'message':'Forbidden.'}
   */
 	/** Access forbidden.*/
 	'Forbidden':{'code': 403, 'id':'Forbidden', 'message':'Forbidden.'},

  /**
   * @apiDefine MeterNotBindCar
   * @apiError MeterNotBindCar Meter does not install to a car.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'MeterNotBindCar', 'message':'Meter does not install to a car.'}
   */
 	/** Access forbidden.*/
 	'MeterNotBindCar':{'code': 403, 'id':'MeterNotBindCar', 'message':'Meter does not install to a car.'},

  /**
   * @apiDefine MeterDiffAgent
   * @apiError MeterDiffAgent Driver can not login to the meter of other agent.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'MeterDiffAgent', 'message':'Driver can not login to the meter of other agent.'}
   */
 	/** Access forbidden.*/
 	'MeterDiffAgent':{'code': 403, 'id':'MeterDiffAgent', 'message':'Driver can not login to the meter of other agent.'},

  /**
   * @apiDefine MeterDiffTaxiGroup
   * @apiError MeterDiffTaxiGroup Driver can not login to the meter of other taxi group.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'MeterDiffTaxiGroup', 'message':'Driver can not login to the meter of other taxi group.'}
   */
 	/** Access forbidden.*/
 	'MeterDiffTaxiGroup':{'code': 403, 'id':'MeterDiffTaxiGroup', 'message':'Driver can not login to the meter of other taxi group.'},

  /**
   * @apiDefine DriverNotLogout
   * @apiError DriverNotLogout Driver not logout.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'DriverNotLogout', 'message':'Driver not logout.'}
   */
 	/** Access forbidden.*/
 	'DriverNotLogout':{'code': 403, 'id':'DriverNotLogout', 'message':'Driver not logout.'},

  /**
   * @apiDefine MeterNotLogout
   * @apiError MeterNotLogout Meter not logout.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'MeterNotLogout', 'message':'Meter not logout.'}
   */
 	/** Access forbidden.*/
 	'MeterNotLogout':{'code': 403, 'id':'MeterNotLogout', 'message':'Meter not logout.'},

   /**
   * @apiDefine OrderNotStart
   * @apiError OrderNotStart Order not start.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code':403, 'id':'OrderNotStart', 'message':'Order not start.'}
   */
 	/** Order not start.*/
 	'OrderNotStart':{'code':403, 'id':'OrderNotStart', 'message':'Order not start.'},

   /**
   * @apiDefine OrderNotEnd
   * @apiError OrderNotEnd Order not end.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code':403, 'id':'OrderNotEnd', 'message':'Order not end.'}
   */
 	/** Order not start.*/
 	'OrderNotEnd':{'code':403, 'id':'OrderNotEnd', 'message':'Order not end.'},

   /**
   * @apiDefine OrderEnd
   * @apiError OrderEnd Order is end.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code':403, 'id':'OrderEnd', 'message':'Order is end.'}
   */
 	/** Order is end.*/
 	'OrderEnd':{'code':403, 'id':'OrderEnd', 'message':'Order is end.'},

   /**
   * @apiDefine OrderClosed
   * @apiError OrderClosed Order is closed.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code':403, 'id':'OrderClosed', 'message':'Order is closed.'}
   */
 	/** Order is closed.*/
 	'OrderClosed':{'code':403, 'id':'OrderClosed', 'message':'Order is closed.'},

  /**
   * @apiDefine Suspend
   * @apiError Suspend Access forbidden.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'Suspend', 'message':'Access forbidden.'}
   */
 	/** Access forbidden.*/
 	'Suspend':{'code': 403, 'id':'Suspend', 'message':'Access forbidden.'},
/**
   * @apiDefine roleIDNotDelete
   * @apiError roleIDNotDelete roleID Not Delete.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 403 Forbidden
   *    {'code': 403, 'id':'roleIDNotDelete', 'message':'roleID Not Delete.'}
   */
 	/** Access forbidden.*/
 	'roleIDNotDelete':{'code': 403, 'id':'roleIDNotDelete', 'message':'roleID Not Delete.'},
  /**
   * @apiDefine ObjectNotFound
   * @apiError ObjectNotFound Object not found.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 404 Not Found
   *    {'code': 404, 'id':'ObjectNotFound', 'message':'Object not found.'}
   */
 	/** Object not found.*/
 	'ObjectNotFound':{'code': 404, 'id':'ObjectNotFound', 'message':'Object not found.'},

  /**
   * @apiDefine DeviceTokenNotFound
   * @apiError DeviceTokenNotFound Device token not found.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 404 Not Found
   *    {'code': 404, 'id':'DeviceTokenNotFound', 'message':'Device token not found.'}
   */
 	/** Object not found.*/
 	'DeviceTokenNotFound':{'code': 404, 'id':'DeviceTokenNotFound', 'message':'Device token not found.'},

  /**
   * @apiDefine ContactNotFound
   * @apiError ContactNotFound Contact not found.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 404 Not Found
   *    {'code': 404, 'id':'ContactNotFound', 'message':'Contact not found.'}
   */
 	/** Object not found.*/
 	'ContactNotFound':{'code': 404, 'id':'ContactNotFound', 'message':'Contact not found.'},

  /**
   * @apiDefine ShiftNotFound
   * @apiError ShiftNotFound Shift not found.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 404 Not Found
   *    {'code': 404, 'id':'ShiftNotFound', 'message':'Shift not found.'}
   */
 	/** Object not found.*/
 	'ShiftNotFound':{'code': 404, 'id':'ShiftNotFound', 'message':'Shift not found.'},

  /**
   * @apiDefine OrderAggregateNotFound
   * @apiError OrderAggregateNotFound Order aggregate without data.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 404 Not Found
   *    {'code': 404, 'id':'OrderAggregateNotFound', 'message':'Order aggregate without data.'}
   */
 	/** Object not found.*/
 	'OrderAggregateNotFound':{'code': 404, 'id':'OrderAggregateNotFound', 'message':'Order aggregate without data.'},

  /**
   * @apiDefine ConflictError
   * @apiError ConflictError Object Conflict.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 409 Conflict
   *    {'code': 409, 'id':'ConflictError', 'message':'Part of the request can\'t complete.'}
   */
  /** Object Conflict.*/
  'ConflictError':{'code':409, 'id':'ConflictError', 'message':'Part of the request can\'t complete.'},

  /**
   * @apiDefine DatabaseError
   * @apiError DatabaseError Internal database error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'DatabaseError', 'message':'Internal database error.'}
   */
 	/** Database error.*/
 	'DatabaseError':{'code':500, 'id':'DatabaseError', 'message':'Internal database error.'},

  /**
   * @apiDefine DatabaseNotReady
   * @apiError DatabaseNotReady Not connect to Database service.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'DatabaseNotReady', 'message':'Not connect to Database service.'}
   */
 	/** Database error.*/
 	'DatabaseNotReady':{'code':500, 'id':'DatabaseNotReady', 'message':'Not connect to Database service.'},

  /**
   * @apiDefine DuplicateUniqueKeyError
   * @apiError DuplicateUniqueKeyError Duplicate key error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':406, 'id':'DuplicateUniqueKeyError', 'message':'Can't assing duplicate key.'}
   */
  /** Database error.*/
  'DuplicateUniqueKeyError':{'code':406, 'id':'DuplicateUniqueKeyError', 'message':'Can\'t assing duplicate key.'},

  /**
   * @apiDefine InternalError
   * @apiError InternalError Unexpected error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'InternalError', 'message':'Unexpected error.'}
   */
 	/** Unexpected error.*/
 	'InternalError':{'code':500, 'id':'InternalError', 'message':'Unexpected error.'},

  /**
   * @apiDefine UnCatchedError
   * @apiError InternalError Uncatched error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'InternalError', 'message':'Unexpected error.'}
   */
 	/** Unexpected error.*/
 	'UnCatchedError':{'code':500, 'id':'UnCatchedError', 'message':'Uncatched error.'},

  /**
   * @apiDefine MqttDisconnect
   * @apiError Mqtt client disconnect from broker.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'MqttDisconnect', 'message':'Mqtt client disconnect from broker.'}
   */
  /** Unexpected error.*/
  'MqttDisconnect':{'code':500, 'id':'MqttDisconnect', 'message':'Mqtt client disconnect from broker.'},

  /**
   * @apiDefine ChildProcessTerminated
   * @apiError ChildProcessTerminated Child process been terminated.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Child process been terminated
   *    {'code':500, 'id':'ChildProcessTerminated', 'message':'Child process been terminated.'}
   */
  /** Unexpected error.*/
  'ChildProcessTerminated':{'code':500, 'id':'ChildProcessTerminated', 'message':'Child process been terminated.'},
  /**
   * @apiDefine NoPortAvailable
   * @apiError No port available for broker.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 No port available
   *    {'code':500, 'id':'NoPortAvailable', 'message':'No port available for broker.'}
   */
  /** Unexpected error.*/
  'NoPortAvailable':{'code':500, 'id':'NoPortAvailable', 'message':'No port available for broker.'},
  /**
   * @apiDefine ArduinoFwTooBig
   * @apiError Firmware size is too big.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Firmware size is too big.
   *    {'code':500, 'id':'ArduinoFwTooBig', 'message':'Firmware size is too big.'}
   */
  /** Unexpected error.*/
  'ArduinoFwTooBig':{'code':500, 'id':'ArduinoFwTooBig', 'message':'Firmware size is too big.'},
  /**
   * @apiDefine ArduinoFwTooBig
   * @apiError Firmware size is too big.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Firmware size is too big.
   *    {'code':500, 'id':'ArduinoFwTooBig', 'message':'Firmware size is too big.'}
   */
  /** Unexpected error.*/
  'ArduinoFwGeneralError':{'code':500, 'id':'ArduinoFwGeneralError', 'message':'Arduino Firmware Api got something wrong.'},

  /**
   * @apiDefine EncryptError
   * @apiError EncryptError Encrypt error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'EncryptError', 'message':'Encrypt error.'}
   */
 	/** Encrypt error.*/
 	'EncryptError':{'code':500, 'id':'EncryptError', 'message':'Encrypt error.'},

  /**
   * @apiDefine DecryptError
   * @apiError DecryptError Encrypt error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'DecryptError', 'message':'Decrypt error.'}
   */
 	/** Decrypt error.*/
 	'DecryptError':{'code':500, 'id':'DecryptError', 'message':'Decrypt error.'},

  /**
   * @apiDefine ReCalculateShiftError
   * @apiError ReCalculateShiftError Re calculate shift error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'ReCalculateShiftError', 'message':'Decrypt error.'}
   */
 	/** ReCalculateShiftError error.*/
 	'ReCalculateShiftError':{'code':500, 'id':'ReCalculateShiftError', 'message':'Cannot find any Shift by orderID.'},

  /**
   * @apiDefine FCMError
   * @apiError FCMError FCM respond an error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 409 FCM Error
   *    {'code': 409, 'id':'FCMError', 'message':'FCM error.'}
   */
 	/** Third party error.*/
 	'FCMError':{'code': 409, 'id':'FCMError', 'message':'FCM error.'},   

   /**
   * @apiDefine GenerateCSVError
   * @apiError GenerateCSVError Generate csv error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 500 Internal Server Error
   *    {'code':500, 'id':'GenerateCSVError', 'message':'Generate csv error.'}
   */
 	/** Decrypt error.*/
 	'GenerateCSVError':{'code':500, 'id':'GenerateCSVError', 'message':'Generate csv error.'},
  
   /**
   * @apiDefine PaymentError
   * @apiError PaymentError Third party payment service report error.
   * @apiErrorExample {json} Error-Response:
   *    HTTP/1.1 409 Conflict
   *    {'code':409, 'id':'PaymentError', 'message':'Third party payment service report error.'}
   */
 	/** Third party error.*/
 	'PaymentError':{'code':409, 'id':'PaymentError', 'message':'Third party payment service report error.'},  
};

var SuccessCode ={
  /**
   * @apiDefine Created
   * @apiSuccess Created Action in queue.
   * @apiSuccessExample {json} Success-Response:
   *    HTTP/1.1 201 Created
   *    {'code':201, 'id':'Created', 'message':'Action in queue.'}
   */
  /** Action in queue.*/
  'Created':{'code':201, 'id':'Created', 'message':'Action in queue.'},
  /**
   * @apiDefine Accepted
   * @apiSuccess Acceptd accept the Action.
   * @apiSuccessExample {json} Success-Response:
   *    HTTP/1.1 202 Accepted
   *    {'code':202, 'id':'Accepted', 'message':'Action accepted.'}
   */
  /** Action in queue.*/
  'Accepted':{'code':202, 'id':'Accepted', 'message':'Action accepted.'},  
  /**
   * @apiDefine NoContent
   * @apiSuccess NoContent Update content success and no content return.
   * @apiSuccessExample {json} Success-Response:
   *    HTTP/1.1 204 No Contect
   *    {'code':204, 'id':'NoContent', 'message':'Success no content.'}
   */
  /** Success no content.*/
  'NoContent':{'code':204, 'id':'NoContent', 'message':'Success no content.'},
};


exports.ErrorCode = ErrorCode;
exports.SuccessCode = SuccessCode;
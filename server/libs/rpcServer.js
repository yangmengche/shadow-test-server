'use strict'
const rpc = require('json-rpc2');
const config = require('../config/appConfig.json');
const http = require('http');
const assert = require('assert')
const diff = require('deep-diff').diff;
let server = rpc.Server.$create();

function routeTest(args, opt, callback) {
	const srcRes = args[1];
	let options = {
		hostname: config.target.host,
		port: config.target.port,
		path: args[0].path,
		method: args[0].method,
		headers: {
			"Content-Type": "application/json"			
		}
	}
	if(!!args[0].headers.accesskey){
		options.headers.accesskey = args[0].headers.accesskey;
	}
	if(!!args[0].headers.hashkey){
		options.headers.hashkey = args[0].headers.hashkey;
	}	
	if(!!args[0].headers.timestamp){
		options.headers.timestamp = args[0].headers.timestamp;
	}	
	if(!!args[0].headers.accesstoken){
		options.headers.accesstoken = args[0].headers.accesstoken;
	}		
	// console.debug(options);
	if (!options.path || !options.method || !options.headers) {
		console.warn('payload format error');
		return;
	}
	callback();	
	let req = http.request(options, (res) => {
		res.setEncoding('utf8');
		let data = '';

		res.on('data', (chunk) => {
			data += chunk;
			// console.info(`body: ${chunk}`);
		});
		res.on('error', (err) => {
			console.err(err);
		});
		res.on('end', () => {
			// compare response
			try {
				assert.strictEqual(res.statusCode, srcRes.statusCode);
				let dif = diff(JSON.parse(data), srcRes.body)
				assert(!dif, JSON.stringify(dif));
				console.log('[path]:', req.path, ' --> pass');
			} catch (err) {
				console.log('[path]:', req.path, ' --> NG');
				console.error(err.message);
				console.debug('src = ', JSON.stringify(srcRes.body));
				console.debug('test = ', data);
			}
		});
	});
	req.on('error', (err) => {
		console.log(err);
	});

	let data = JSON.stringify(args[0].body);
	req.write(data);
	req.end();
}

server.expose('shadowTest', {
	'routeTest': routeTest
});


console.info('shadow test rpc server created. port=', config.rpc.port);

server.listen(config.rpc.port, '0.0.0.0');